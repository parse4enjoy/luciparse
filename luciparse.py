#coding=utf8

import requests
import fake_useragent
from bs4 import BeautifulSoup

import sys
import os
import re
import datetime
import traceback
from threading import Thread


from PyQt5.QtWidgets import (QApplication, QWidget, QFrame, QLabel, QCheckBox, QComboBox, QTextEdit, QLineEdit, QPushButton,
    QGridLayout, QMessageBox, QFileDialog)

#Req mudules:
#requests
#bs4
#lxml
#fake-useragent
###multiprocessing
#PyQt5
#requests[socks] 
#pysocks



class All:
	thread_running_count = 0

class MainWidget(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.path_choosen = False

        self.__grid = QGridLayout()

        self.__infoText_label = QLabel("info: ", self)
        self.__errorText_label = QLabel("errors: ", self)
        self.__linkLine_label = QLabel("Input link: ", self)
        self.__proxyLine_label = QLabel("Input proxy (ip:port): ", self)
        self.__dirLine_label = QLabel("Choose out directory: ", self)

        self.__infoText = QTextEdit(self)
        self.__infoText.setReadOnly(True)
        self.__errorText = QTextEdit(self)
        self.__errorText.setReadOnly(True)

        self.__linkLine = QLineEdit(self)
        self.__linkLine.textChanged.connect(lambda:self.__if_link_changed())
        self.__proxyLine = QLineEdit(self)
        self.__proxyComboBox_vars = ["None", "SOCKS5", "HTTP/HTTPS"] # Dont change!
        self.__proxyComboBox = QComboBox(self)
        self.__proxyComboBox.addItems(self.__proxyComboBox_vars)
        self.__checkipButton = QPushButton("Check IP", self)
        self.__checkipButton.clicked.connect(lambda:self.__checkipbutton_hundler())

        self.__dirLine = QLineEdit(self)
        self.__dirButton = QPushButton("Choose dir", self)
        self.__dirButton.clicked.connect(lambda:self.__dir_button_hundler())

        self.__startButton = QPushButton("Start", self)
        self.__startButton.clicked.connect(lambda:self.__start_button_hundler())

        c = 0

        self.__grid.addWidget(self.__linkLine_label, c, 0, 1, 1)
        self.__grid.addWidget(self.__linkLine, c, 1, 1, 3)
        c+=1

        self.__grid.addWidget(self.__proxyLine_label, c, 0, 1, 1)
        self.__grid.addWidget(self.__proxyLine, c, 1, 1, 1)
        self.__grid.addWidget(self.__proxyComboBox, c, 2, 1, 1)
        self.__grid.addWidget(self.__checkipButton, c, 3, 1, 1)
        c+=1

        self.__grid.addWidget(self.__dirLine_label, c, 0, 1, 1)
        self.__grid.addWidget(self.__dirLine, c, 1, 1, 2)
        self.__grid.addWidget(self.__dirButton, c, 3, 1, 1)
        c+=1

        self.__grid.addWidget(QLabel("", self), c, 0, 1, 4)
        c+=1

        self.__grid.addWidget(self.__infoText_label, c, 0, 1, 2)
        self.__grid.addWidget(self.__infoText, c+1, 0, 1, 2)

        self.__grid.addWidget(self.__errorText_label, c, 2, 1, 2)
        self.__grid.addWidget(self.__errorText, c+1, 2, 1, 2)
        c+=2

        self.__grid.addWidget(self.__startButton, c, 0, 1, 4)

        self.setLayout(self.__grid)
        self.show()

    def __dir_button_hundler(self):
        dir_path = QFileDialog.getExistingDirectory(self, "Select directory", str(os.getcwd()))
        if(dir_path == ""):
            return
        self.__dirLine.setText(dir_path)
        self.path_choosen = True

    def __checkipbutton_hundler(self):
        proxy_content_index = self.__proxyComboBox.currentIndex()
        m = re.findall(r'[0-9]+(?:\.[0-9]+){3}:[0-9]+', self.__proxyLine.text())
        if(proxy_content_index != 0 and len(m) != 1):
            self.show_message("Fill proxy field", 4)
        else:
            ip_text_1 = self.get_responce_as_text("https://ifconfig.me/ip")
            ip_text_2 = self.get_responce_as_text("https://ipinfo.io/ip")
            ip_text_3 = self.get_responce_as_text("https://icanhazip.com")
            self.show_message(f"Your ip: \n{ip_text_1} \n{ip_text_2} \n{ip_text_3} \n", 2)

    def __if_link_changed(self):
        if(self.path_choosen == False):
            self.__dirLine.setText(self.__linkLine.text())

    def __start_button_hundler(self):
        if(All.thread_running_count != 0):
            self.show_message(f"Previous parse do not complete! ", 4)
            return
        if(self.check_fields_data() == False):
            return

        dir_name = self.__dirLine.text()
        if(os.path.exists(dir_name) == False):
            os.mkdir(dir_name)
        else:
            if(os.path.isdir(dir_name) == False):
                self.show_message(f"\"{dir_name}\" is not directory! ", 4)
                return
            else:
                if( len(os.listdir(dir_name)) != 0 ):
                    self.show_message(f"\"{dir_name}\" is not empty! ", 4)
                    return
        
        do_requests(self.__linkLine.text(), dir_name, self)

        #responce_text = self.get_responce_as_text(self.__linkLine.text())
        #self.append_info(responce_text)

    def check_fields_data(self) -> bool:
        if(self.__linkLine.text() == ""):
            self.show_message("Fill link field", 4)
            return False

        if(self.__dirLine.text() == ""):
            self.show_message("Choose output path", 4)
            return False

        proxy_content_index = self.__proxyComboBox.currentIndex()
        m = re.findall(r'[0-9]+(?:\.[0-9]+){3}:[0-9]+', self.__proxyLine.text())
        if(proxy_content_index != 0 and len(m) != 1):
            self.show_message("Fill proxy field", 4)
            return False


    def get_proxy(self) -> dict:
        proxy_content_index = self.__proxyComboBox.currentIndex()
        if(proxy_content_index == 0):
            return None

        proxy_text = self.__proxyLine.text()
        m = re.findall(r'[0-9]+(?:\.[0-9]+){3}:[0-9]+', proxy_text)
        if(len(m) == 1):
            if(proxy_content_index == 1):
                return {"http": f"socks5://{m[0]}", "https": f"socks5://{m[0]}"}
            elif(proxy_content_index == 2):
                return {"http": f"http://{m[0]}", "https": f"https://{m[0]}"}
            else:
                return {"http": f"error://123.123.123.123:123", "https": f"errors://321.321.321.321:321"}
        else:
            return {"http": f"error://123.123.123.123:123", "https": f"errors://321.321.321.321:321"}

    def do_request(self, link: str) -> "response":
        try:
            r = requests.get(link, headers=get_head(), proxies=self.get_proxy())
            return r
        except:
            traceback.print_exc()
            self.show_message(f"Cannot do request on link: \"{link}\". ", 4)
            return None

    def get_responce_as_text(self, link: str) -> str:
        r = self.do_request(link)
        if(r == None):
            return None
        response = r.text
        return response

    def get_responce_as_soup(self, link: str) -> "BeautifulSoup":
        response_text = self.get_responce_as_text(link)
        if(response_text == None):
            return None
        soup = BeautifulSoup(response_text, "lxml")
        return soup

    def append_info(self, text: str):
        self.__infoText.append(f"\n{getTime()}: \n{text}")

    def show_message(self, text: str, type: int, ifMODAL:bool = True):
        # types:
            # 0 - None
            # 1 - Question
            # 2 - Information
            # 3 - Warning
            # 4 - Critical
        m = QMessageBox(self)

        if(type == 0):
            m.setWindowTitle("")
        elif(type == 1):
            m.setIcon(QMessageBox.Question)
            m.setWindowTitle("Question")
        elif(type == 2):
            m.setIcon(QMessageBox.Information)
            m.setWindowTitle("Info")
        elif(type == 3):
            m.setIcon(QMessageBox.Warning)
            m.setWindowTitle("Warning")
        elif(type == 4):
            m.setIcon(QMessageBox.Critical)
            m.setWindowTitle("Error")

        m.setText(text)
        m.setStandardButtons(QMessageBox.Ok)
        m.setModal(ifMODAL)
        m.exec()

def getTime() -> str:
    time_str = datetime.datetime.now().strftime("[%d.%m.%y %H:%M:%S.%f]")
    return time_str

def get_head() -> dict:
    user = fake_useragent.UserAgent(verify_ssl=False).random
    header = {"user-agent" : user}
    return header


def do_requests(link_fu: str, out_folder: str, widget: "MainWidget"):
    All.thread_running_count += 1
    cpus = os.cpu_count()
    threads = []
    for i in range(cpus):
        t = Thread(target=do_one_job, args=([link_fu], out_folder, widget, ))
        threads.append(t)
        t.start()

    All.thread_running_count -= 1

def do_one_job(links: list, out_folder: str, widget: "MainWidget"):
    All.thread_running_count += 1

    for link_i in links:
        pass

    All.thread_running_count -= 1

if __name__ == '__main__':
    All.thread_running_count = 0
    app = QApplication(sys.argv)

    mainWidget = MainWidget()
    mainWidget.setWindowTitle("luciparse")

    sys.exit(app.exec_())
